local pegasus = require ('pegasus')
local Table = require ("Server/Lib/Table")
local Http = require("Server/Lib/Http")

local routing = {
  ["/"] = require("Server/Controller/Home"),
  ["/Cookies"] = require("Server/Controller/CustomCookie"),
  ["/form"] = require("Server/Controller/Form")
}


_G.count = 0

pegasus:new{ port='9090' }:start(function (request, response)  

  local path = request:path()
  if path == nil then
      return 
  end  

  print("## Request recieved " .. path .. " ##")  
  local controller = routing[request:path()] 
  if controller ~= nil then    
      local method = controller[request._method]
      if method ~= nil then                    
          --request:headers()          
          
          request = Http.preprocessRequest(request)          
          response = Http.preprocessResponse(response)          
          print("<req>")
          print(Table.tostring(request))
          print("</req>")
          method(request, response)
          
          response:finalizeCookies()
          response:finalizeBody()
      end
  else
      print("Request not served")
  end
  
  --print(Table.tostring(response))
  print("## Done ##")
  print()
end)

