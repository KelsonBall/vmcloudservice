local args = {...}
local lfs = require("lfs")

if args[1] == "create" then
    lfs.mkdir("Client")
    lfs.mkdir("Client/www")
    lfs.mkdir("Client/Lib")
    lfs.mkdir("Client/Templates")
    lfs.mkdir("Server")
    lfs.mkdir("Server/Model")
    lfs.mkdir("Server/View")
    lfs.mkdir("Server/Controller")
    lfs.mkdir("Server/Repository")
    lfs.mkdir("") 
end