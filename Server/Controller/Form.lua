-- Module
local Controller = {}

-- Packages
local template = require ("resty.template")
local Http = require("Server/Lib/Http")
--local FormModel = require("Server/Model/FormModel")

-- Views
template.caching(false)
local formView = template.compile("Client/www/form.resty")   

-- Local functions


-- Http methods
function Controller.GET(request, response)
    
    -- Process query 
    local query = Http.query(request)   
    for k,v in pairs(query or {}) do
        print(k,v)
    end
    

    -- build page
    local page = formView({ title = "Get"})        

    -- write page to response
    response:writeBody(page)    
end

function Controller.POST(request, response)
    -- Process query 
    local query = Http.query(request)   
    for k,v in pairs(query or {}) do
        print(k,v)
    end

    
    --print(data)
    

    -- build page
    local page = formView({ title = "Post"})        

    -- write page to response
    response:writeBody(page)
end

return Controller