-- Module
local Controller = {}

-- Packages
local template = require("resty.template")
local Table = require("Server/Lib/Table")
local Http = require("Server/Lib/Http")
local CookieModel = require("Server/Model/CookieModel")

-- Views
template.caching(true)
local cookieFormView = template.compile("Client/www/cookies.resty")

-- Local functions
function AddCookie(response, cookieName, cookieData)

end

function RemoveCookie(response, cookieName)

end

-- Http methods
function Controller.GET(request, response)    
    if request.Cookies ~= nil then
        print("Cookies")
        for k,v in pairs(request.Cookies) do
            print(k,v)
        end
    else 
        print("No Cookies")
    end

    response:addCookie("count", _G.count)   

    local llamas = request:tryGetCookie("llamas", 0) + 1
    if llamas > 3 then
        response:deleteCookie("llamas")
    else
        response:addCookie("llamas", llamas)
    end     

    response:writeBody("Cookie page")        
end

return Controller
