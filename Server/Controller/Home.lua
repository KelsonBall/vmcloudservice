-- Module
local Controller = {}

-- Packages
local template = require ("resty.template")
local Http = require("Server/Lib/Http")
local HomeModel = require("Server/Model/HomeModel")

-- Views
template.caching(true)
local homeView = template.compile("Client/www/index.resty")   

-- Local functions
-- manageCounter manipulates the global count variable
function manageCounter(query)
    if query.set ~= nil then
        local v = tonumber(query.set)
        if v ~= nil then
            _G.count = v
        end
    end

    if query.add ~= nil then        
        _G.count = _G.count + ( tonumber(query.add) or 0)
    end

    if query.sub ~= nil then
        _G.count = _G.count - ( tonumber(query.sub) or 0)
    end
end


-- Http methods
function Controller.GET(request, response)
    
    -- Process query    
    manageCounter(Http.query(request))

    -- build model
    local model = HomeModel:new(_G.count)

    -- build page
    local page = homeView(model)    

    response:deleteCookie("llamas")

    -- write page to response
    response:writeBody(page)    
end

return Controller