local HomeModel = { title   = "Testing lua-resty-template", names = { "One", "Two", "Three" }}

function HomeModel:new(count)
    local t = {}    
    t.message = "Hello, World! Count = " .. count,
    setmetatable(t, HomeModel)
    self.__index = self
    return t
end

return HomeModel