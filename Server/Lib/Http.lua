local http = {}

function http.split(text, seperator)
    local result = {}
    if text == nil or seperator == nil then
        return ""
    end
    local index = string.find(text, seperator) 
    while index do        
        result[#result + 1] = string.sub(text, 0, index - 1)
        text = string.sub(text, index + 1)        
        index = string.find(text, seperator)
    end
    result[#result + 1] = text
    return result
end

function http.query(request)
    local queryString = request._query_string
    local t = http.split(queryString, "&")
    local retval = {}
    for k,v in pairs(t) do
        local keyValuePair = http.split(v,"=")        
        retval[keyValuePair[1]] = keyValuePair[2]
    end
    return retval
end

function getPostData(request)
    if request._method == "POST" then
        request.client:settimeout(0)
        local data, err, partial = request.client:receive(_content_length - _content_done)
        print(data, err, partial)

        if err =='timeout' then
            err = nil
            data = partial
        end

        request._content_done = request._content_done + #data

        request.post = data
        print("Post data: ")
        print(type(data))
        print(data)
        print(string.len(data))
    end
end

function http.preprocessRequest(request)
    request.data = {}    

    local cookieItems = {}
    function request:tryGetCookie(name, default)
        if cookieItems[name] ~= nil then
            return cookieItems[name]
        else
            return default
        end
    end    

    local line = request.client:receive()
    while line ~= nil do 
        kvp = http.split(line, ":")        
        if kvp[1] == "Cookie" then            
            if string.sub(kvp[2], 1, 5) == " data" then
                local splitString = string.sub(kvp[2], 7)                
                local cookies = http.split(splitString, "&")                
                for i=1, #cookies do
                    local kvp = http.split(cookies[i], "=")
                    cookieItems[kvp[1]] = kvp[2]
                end                
                request.Cookies = cookieItems            
            end
        elseif kvp[1] == "Content-Length" then
            print("Content length: " .. kvp[2])
            request._content_length = tonumber(kvp[2])            
        end        
        
        items = http.split(kvp[2], ",")              

        -- strip leading spaces      
        for i=1,#items do
            if string.sub(items[i], 1, 1) == " " then
                items[i] = string.sub(items[i], 2)
            end
        end

        request.data[kvp[1]] = items
        line = request.client:receive()
    end    

    if request._content_length ~= nil then
        getPostData(request) 
    end

    return request
end

function http.preprocessResponse(response)
    local finalized = false
    local pendingCookies = response.request.Cookies or {}

    function response:addCookie(name, value)
        if finalized then
            return nil, "Cookies already finalized."
        end        

        pendingCookies[name] = value

        return self   
    end

    function response:deleteCookie(name)
        if finalized then
            return nil, "Cookies already finalized."
        end

        pendingCookies[name] = nil

        return self
    end

    function response:finalizeCookies()
        finalized = true
        if pendingCookies ~= nil then
            local cookieString            
            for k,v in pairs(pendingCookies) do                
                if cookieString ~= nil then
                    cookieString = cookieString .. "&"
                end
                cookieString = (cookieString or "") .. k .. "=" .. v
            end            
            if cookieString ~= nil then
                self:addHeader("Set-Cookie", "data=" .. cookieString)
            end
        end

        return self
    end

    local body = ""
    function response:writeBody(data)
        body = body .. data
    end

    function response:finalizeBody()        
        response:addHeader('Content-Type', 'text/html'):write(body)
    end

    return response
end

return http