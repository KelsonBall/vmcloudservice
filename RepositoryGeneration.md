# Repository Automation and Generation

## Schema Definition

Repository models must contain a schema property that contains a map of column names to data types.

Allowed data types are:
* `text`
* `integer`
* `real`
* `data`
* `key`
* `date`

Columns that are nullable are sufixed with `?`

Example:

        Person = {
            schema = {
                FirstName = "text",
                LastName = "text?",
                Age = "integer?",
                Score = "real",
                Avatar = "data?",
                Father = "key?",
                Mother = "key?",
                Birthday = "date?"                
            }
        }

This model schema would be used to generate the necessary database interface.
Model schemas have an implied Id = "key" field.

## Repositories

Repositories expose the following operations

* `create(model) -> repository`  
* `read(query) -> results`
* `update(model) -> repository` 
* `delete(model) -> repository`

